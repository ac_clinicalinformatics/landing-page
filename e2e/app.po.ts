import { browser, by, element } from 'protractor';

export class AocLandingPagePage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-header h1')).getText();
  }
}
