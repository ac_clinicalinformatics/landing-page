import { AocLandingPagePage } from './app.po';

describe('aoc-landing-page App', () => {
  let page: AocLandingPagePage;

  beforeEach(() => {
    page = new AocLandingPagePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
