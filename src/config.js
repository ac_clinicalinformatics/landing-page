(function (global) {
    System.config({
        transpiler: 'typescript', 
        typescriptOptions: { emitDecoratorMetadata: true }, 
        //
        // We need to define a package for each of the applications we want to 
        //  run on the page
        //
        packages: {
          'app-header': {defaultExtension: 'ts'},
          'app-vitals': {defaultExtension: 'ts'}  
        } 
      });
})(this);