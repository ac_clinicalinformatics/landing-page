import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppMainModule } from './app-main/app.main.module';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// IE requires we wait for DOM to finish loading otherwise there is 'ng' undefined error
window.addEventListener('load', function() {
  platformBrowserDynamic().bootstrapModule(AppMainModule);
});
