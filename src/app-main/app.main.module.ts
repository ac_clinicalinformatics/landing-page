import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { HttpModule } from '@angular/http';

import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
import { MdCardModule, MdGridListModule, MdExpansionModule, MdSliderModule, MdTooltipModule, MdButtonModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { HeaderComponent } from './components/header.component';
import { ContentComponent } from './components/content.component';

import { VitalsComponent } from './components/content/vitals.component';
import { AboutMeComponent } from './components/content/aboutme.component';
import { WhyAmIHereComponent } from './components/content/whyamihere.component';
import { TimelineComponent } from './components/content/timeline.component';
import { DataComponent } from './components/content/data.component';
import { MedsComponent } from './components/content/meds.component';
import { ProblemsComponent } from './components/content/problems.component';
import { OrdersComponent } from './components/content/orders.component';
import { AlertsComponent } from './components/content/alerts.component';
import { NotesToSelfComponent } from './components/content/notestoself.component';

import { CentricityApiService } from './services/centricity_api.service'
import { CentricityRulesService } from './services/centricity_rules.service'

// import { VisModule } from 'ng2-vis';
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'app-main',
  template: `
    <div id="wrapper">
        <div id="headerwrap">
          <div id="header">
              <app-header>Loading header...</app-header>
          </div>
        </div>
        <div id="contentwrap">
          <div id="content">
            <app-content></app-content>
          </div>
        </div>
    </div>
  `
})
export class AppComponent {
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    VitalsComponent,
    AboutMeComponent,
    WhyAmIHereComponent,
    TimelineComponent,
    DataComponent,
    MedsComponent,
    ProblemsComponent,
    OrdersComponent,
    AlertsComponent,
    NotesToSelfComponent,
    ContentComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    HttpModule,
    MdButtonModule,
    MdTooltipModule,
    Angular2FontawesomeModule,
    MdSliderModule,
    MdExpansionModule,
    MdCardModule,
    MdGridListModule,
    NoopAnimationsModule,
    BrowserModule,
    ChartsModule
  ],
  providers: [CentricityApiService, CentricityRulesService],
  bootstrap: [AppComponent]
})
export class AppMainModule { }
