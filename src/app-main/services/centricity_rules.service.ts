import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { CentricityApiService } from '../services/centricity_api.service'
import { AngularCompilerOptions } from '@angular/compiler-cli';

// Imported from json-logic-js
declare var jsonLogic: any;

@Injectable()
export class CentricityRulesService {
  ruledata = [];
  centricityApi: any = null;

  constructor ( private http: Http, private centricityApiService: CentricityApiService) {
    this.centricityApi = centricityApiService;
  }

  private cacheRuleSet(ruleset, json, pvidFilter, groupidFilter) {
    const cachedRuleset = [];

    for (const rule of json) {
      let isFiltered = false;

      if (rule.pvid && rule.pvid !== pvidFilter) {
        isFiltered = true;
      }

      if (!isFiltered && rule.groupid && rule.groupid !== groupidFilter) {
        isFiltered = true;
      }

      if (!isFiltered) {
        cachedRuleset.push(rule);
      }
    }

    // alert(JSON.stringify(cachedRuleset));
    this.ruledata.push({ name: ruleset, data: cachedRuleset });
  }


  loadRuleset(ruleset, callback, pvidFilter, groupidFilter) {
    if (this.ruledata.indexOf(r => r.ruleset === 'ruleset') < 0) {
      const url = 'assets/rules/' + ruleset + '.json';

      this.http.get(url).subscribe(data => {
        // Read the result field from the JSON response and filters
        this.cacheRuleSet(ruleset, data.json(), pvidFilter, groupidFilter);

        if (callback) {
          callback(this.getRulesetDetails(ruleset));
        }
      },
      err => {
        alert(err);
      });
    }
  }

  getRulesetDetails(ruleset) {
    return this.ruledata.find(x => x.name === ruleset);
  }

  runSimpleDataRule(rulelogic, ruledata) {
    if (rulelogic && ruledata) {
      jsonLogic.apply(rulelogic, ruledata);
    }

    return jsonLogic.apply(rulelogic, ruledata);
  }

  runMelDataRule(rulelogic, melruledata, melprecursorfunc) {
    let copyruledata = null;

    if (rulelogic && melruledata) {
      copyruledata = Object.assign({}, melruledata);

      // Evaluate the MEL
      for (const key in copyruledata) {
        if (copyruledata.hasOwnProperty(key)) {
          // Prepend prescursor function if provided
          const melExpression = melprecursorfunc ? melprecursorfunc + copyruledata[key] : copyruledata[key];
          const melResult = this.centricityApi.evaluateMel(melExpression);

          // If numeric then convert it else treat as string
          if (+melResult !== NaN) {
            copyruledata[key] = +melResult;
          } else {
            /* if (Object.prototype.toString.call(melResult) === '[object Date]') {
              copyruledata[key] = new Date(melResult);
            } */

            copyruledata[key] = melResult;
          }
        }
      }

      return jsonLogic.apply(rulelogic, copyruledata);
    }
  }
}
