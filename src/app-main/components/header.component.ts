import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { CentricityApiService } from '../services/centricity_api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  patientPicture = undefined;
  showPicture = false;
  formId = 0;
  patientDemographics: any = null;
  patientEthnicity = '';
  patientInsurance = '';
  patientPCP = '';
  patientCareTeam = '';
  patientNextAppt = '';
  riskIndicatorValue = null;
  riskIndicatorBackground = '';
  patientPhoneNumber = null;
  hoverRiskValue = '';
  unexpiredFlagCount = 0;
  currentFlags = [];
  PatientAgeString = '';
  ptRisk = '';

  constructor(private domSanitizer: DomSanitizer, private centricityApi: CentricityApiService) {
    this.patientDemographics = centricityApi.getPatientDemographics(false);
    const pic = centricityApi.convertBinaryBmp(this.patientDemographics.patientPicture);

    this.patientEthnicity = centricityApi.evaluateMel('{PATIENT.ETHNICITY}');
    this.patientInsurance = centricityApi.evaluateMel('{INS_NAME("P")}');

    const pcp = centricityApi.evaluateMel('{PATIENT.PCP}');
    this.patientPCP = pcp !== '' ? pcp : '<None>';

    const careTeam = centricityApi.evaluateMel('{OBSANY("CARETEAMNM")}');
    this.patientCareTeam = careTeam !== '' ? careTeam : '<None>';

    this.patientNextAppt = centricityApi.evaluateMel('{APPT_NEXT()}');

    this.ptRisk = centricityApi.evaluateMel('{OBSANY("PATIENTRISK")}');
//    const patientRisk = centricityApi.evaluateMel('{OBSANY("PATIENTRISK")}');
    this.hoverRiskValue = centricityApi.evaluateMel('{OBSANY("PATIENTRISK")}');
    this.riskIndicatorValue = this.ptRisk !== '' ? this.ptRisk[0] : '?';
    this.setRiskIndicatorStyle();
    this.prefNum();
    this.adjPatientAgeString(this.patientDemographics.personAge.AgeInMonths);

    this.patientPicture = domSanitizer.bypassSecurityTrustUrl(pic);
    this.formId = centricityApi.getFormId();
    this.showPicture = this.patientPicture && this.formId > 0;

    this.getFlagCountFromOtherProviders();
  }

  UpdatePtRisk(val) {
    this.toggleSel()

    if (val){
      this.centricityApi.setObsValue('PATIENTRISK',val);
      this.riskIndicatorValue = val[0]
      this.setRiskIndicatorStyle()
    }
  }

  toggleSel() {
    document.getElementById("riskDropdown").classList.toggle("show");
  }

  prefNum() {
    const cellNum = this.centricityApi.evaluateMel('{PATIENT.CELLPHONE}');

    if (cellNum) {
      this.patientPhoneNumber = cellNum;
    }
    else {
      const workNum = this.centricityApi.evaluateMel('{PATIENT.WORKPHONE}');
      if (workNum !== '') {
        this.patientPhoneNumber = workNum;
      } else {
        return '<None>';
      }
    }
  }

  adjDOB(pdate) {
    pdate = pdate.replace(/(\d\d\d\d)(\d\d)(\d\d)/g, '$2/$3/$1');
    return pdate
  }

  adjPatientAgeString(ageMonths){
    if (ageMonths <=  12){ this.PatientAgeString = ageMonths + 'mths ' }
    else {
      let ptMonths = ageMonths % 12 ;
      let ptYears = (ageMonths-ptMonths) / 12 ;

      if (ptMonths !== 0) {
        this.PatientAgeString = ptYears + ' y/o '+ptMonths + ' mth,';
      }
      else { this.PatientAgeString = ptYears + ' y/o,';}
    }
  }

  getFlagCountFromOtherProviders() {
    this.currentFlags = this.centricityApi.getPatientFlags(false);
    let countResult = 0;

    for (const flag of this.currentFlags) {
      const now = new Date();
      if (!flag.ExpireSpecified) {
        countResult++;
      } else {
        const expiredDate = new Date(this.centricityApi.clinicalDateToString(flag.Expire));
        if (expiredDate > now) {
          countResult++;
        }
      }
    }

    this.unexpiredFlagCount = countResult;
  }

  setRiskIndicatorStyle() {
    switch (this.riskIndicatorValue) {
      case 'L': {
        this.riskIndicatorBackground = 'green';
        break;
      }
      case 'M': {
        this.riskIndicatorBackground = '#CCCC00';
        break;
      }
      case 'H': {
        this.riskIndicatorBackground = 'orange';
        break;
      }
      case 'E': {
        this.riskIndicatorBackground = 'red';
        break;
      }
      default: {
        this.riskIndicatorBackground = 'grey';
        break;
      }
    }
  }
}
