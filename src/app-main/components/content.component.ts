import { Component, ViewChild, ApplicationRef, ElementRef } from '@angular/core';
import { CentricityApiService } from '../services/centricity_api.service'
import { CentricityRulesService } from '../services/centricity_rules.service'
import { MedsComponent } from '../components/content/meds.component';
import { NotesToSelfComponent } from '../components/content/notestoself.component';
import { ChildProcess } from 'child_process';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
  
export class ContentComponent {
  @ViewChild('aboutme') aboutMeTile: ElementRef;
  @ViewChild('problems') problemsTile: ElementRef;
  @ViewChild('alerts') alertsTile: ElementRef;
  @ViewChild('orders') ordersTile: ElementRef;
  @ViewChild('meds') medsTile: ElementRef;
  @ViewChild('notesToSelf') notesToSelfTile: ElementRef;
  
  formId = 0;
  patientId = NaN;  
  showOnlyThisPanel = '';
  drillablePanels = ['alerts-panel', 'data-panel'];

  // OBS to display
  todayHeight: string = null;
  todayBMI: string = null;
  todayWeight: string = null;
  todayDiastolic: string = null;
  todaySystolic: string = null;
  todayPulseRate: string = null;
  todayTemp: string = null;
  todayO2Sat: string = null;
  todayRespRate: string = null;
  patientAllergiesList = [];
  obsNKAResult = '';
  patientAllergiesString = '';
  patientDemographics: any = null;  
  allergyTextColor = null;
  
  //  medRxfill = 0;
  currentUserPvid = 0;  
  
  collayoutcount = 3;  
  maxrowcount = 14;
  maxexpandrows = 10;
  targetExpandedTiles = [];

  content = 'Example';

  timeline = [];
  
  constructor( private appRef: ApplicationRef, private centricityApi: CentricityApiService, private rulesService: CentricityRulesService) {
    //added for meds
    this.currentUserPvid = +centricityApi.evaluateMel('{USER.PVID}');
  //this.medRxfill = this.getRxCount();
    
    // Keep reference to centricityAPIService
    this.formId = centricityApi.getFormId();
    this.patientDemographics = centricityApi.getPatientDemographics(false);

    this.getObsAndAssignObsValues();
    this.getPatientAllergies(false);
    this.centricityApi.registerForObsNotifications(this.onObsChangedCallback.bind(this));
    this.centricityApi.registerForAllergiesNotifications(this.onAllergiesChangedCallback.bind(this));    
  }
  
  
  melResultCallback(melResult) {
    // All is good?
  }
  
  launchProblems() {
    this.centricityApi.updateProblems(this.melResultCallback.bind(this));
  }
  
  launchMedications() {
    this.centricityApi.updateMedications(this.melResultCallback.bind(this));
  }
  
  launchOrders() {
    this.centricityApi.updateOrders(this.melResultCallback.bind(this));
  }
  
  toggleTileColExpansion(event, tileRef, colSpan, rowSpan , ogrowSpan) {
    if(tileRef) {
      if(tileRef.colspan == 1) {
        tileRef.colspan = colSpan;
    tileRef.rowspan = rowSpan;
        this.targetExpandedTiles.push(tileRef);
      } else {
        tileRef.colspan = 1;
        tileRef.rowspan = ogrowSpan;
        const index: number = this.targetExpandedTiles.indexOf(tileRef);
        if (index !== -1) {
            this.targetExpandedTiles.splice(index, 1);
        }
      }
    }
  }
  
  runSampleRules(ruleset) {
    for (const rule of ruleset.data) {
      const result = this.rulesService.runMelDataRule(rule.jsonLogic.rule, rule.jsonLogic.meldatatemplate, rule.jsonLogic.melprecursorfunc);
      alert(rule.name + ' result: ' + result);
    }
  }
  
  getPatientAllergies(loadLatest) {
    this.patientAllergiesList.splice(0, this.patientAllergiesList.length);
    this.patientAllergiesString = '';   
    this.allergyTextColor = '';
    this.obsNKAResult = this.centricityApi.evaluateMel('{OBSANY("NKA")}');
    
    const allergies = this.centricityApi.getPatientAllergies(loadLatest);
    
    if (allergies && allergies.length > 0) {
      for (const allergy of allergies) {
        
        this.allergyTextColor = '#FF0000';
        
        if (this.centricityApi.isSignedOrInCurrentUpdateItem(allergy) && this.centricityApi.isActiveItem(allergy)) {
        
          this.patientAllergiesList.push({ name: allergy.name });
          
          if (this.patientAllergiesString.length > 0) {
            this.patientAllergiesString += ', ';
          }
          this.patientAllergiesString += allergy.name;
          
        }
      }      
    } else {
       if (this.obsNKAResult) {
                     this.patientAllergiesString= 'None';
                     this.allergyTextColor = 'black';
          }  else {
                    this.patientAllergiesString = '?';
                    this.allergyTextColor = 'orange';
                  }   
    }
  }
  
  getObsAndAssignObsValues() {

    this.centricityApi.obsNow('Weight', this.onTodayWeightCallback.bind(this));
    this.centricityApi.obsNow('Height', this.onTodayHeightCallback.bind(this));
    this.centricityApi.obsNow('Bmi', this.onTodayBMICallback.bind(this));

    this.centricityApi.obsNow('Pulse Rate', this.onTodayPulseRateCallback.bind(this));
    this.centricityApi.obsNow('Temperature', this.onTodayTemperatureCallback.bind(this));
    this.centricityApi.obsNow('O2Sat(OXIM)', this.onTodayO2Callback.bind(this));
    this.centricityApi.obsNow('Resp Rate', this.onTodayRespRateCallback.bind(this));
    this.centricityApi.obsNow('BP Diastolic', this.onTodayBpDiastolicCallback.bind(this));
    this.centricityApi.obsNow('BP Systolic', this.onTodayBpSystolicCallback.bind(this));
  }

  // Event handlers
  onPanelChange(event) {
    if (this.isDrillablePanel(event.panelId)) {
      if (event.nextState) {
        // If expanded
        this.showOnlyThisPanel = event.panelId;
      } else if (this.showOnlyThisPanel === event.panelId && !event.nextState) {
        // Collapsing the panelToggled now show all of them
        this.showOnlyThisPanel = '';
      }
    } else {
      this.showOnlyThisPanel = '';
    }
  }

  isDrillablePanel(panelName) {
    return this.drillablePanels.indexOf(panelName) >= 0;
  }

  onGoBack(acc) {
    acc.toggle(this.showOnlyThisPanel);
  }

  // Allergies callback
  onAllergiesChangedCallback() {
    this.getPatientAllergies(true);
  }
    
  // OBS callback
  onObsChangedCallback() {
    this.getObsAndAssignObsValues();
  }

  onTodayBpDiastolicCallback(value) {
    this.todayDiastolic = value !== '' ? value : '-';
    this.appRef.tick();
  }

  onTodayBpSystolicCallback(value) {
    this.todaySystolic = value !== '' ? value : '-';
    this.appRef.tick();
  }

  onTodayRespRateCallback(value) {
    this.todayRespRate = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayO2Callback(value) {
    this.todayO2Sat = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayPulseRateCallback(value) {
    this.todayPulseRate = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayTemperatureCallback(value) {
    this.todayTemp = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayBMICallback(value) {
    this.todayBMI = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayHeightCallback(value) {
    this.todayHeight = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayWeightCallback(value) {
    this.todayWeight = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }
  
}
