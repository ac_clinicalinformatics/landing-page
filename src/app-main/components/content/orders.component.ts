import { Component } from '@angular/core';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent {
  title = 'orders';
  patientReferalOrderList = [];
  patientTestOrderList = [];
  
  constructor(private centricityApi: CentricityApiService) {
    this.centricityApi.getPatientActiveReferralAndTestOrdersAndCallback(this.patientOrdersCallback.bind(this));
  }
  
  patientOrdersCallback(orders) {
    const referralStart = orders.indexOf('R|') + 2;
    const testStart = orders.indexOf('T|') + 2;
    let testSubstring = '';
    let referralSubstring = '';
    let testRows = [];
    let referralRows = [];
    
    if (referralStart >= 0) {
      // Has both Tests and Referrals
      if (testStart > 0) {
        referralSubstring = orders.substr(referralStart, testStart - 4);
        testSubstring = orders.substr(testStart);
      } else {
        // Only has Referrals
        referralSubstring = orders.substr(testStart);
      }
    } else if (testStart >= 0) {
      // Only has Tests
      testSubstring = orders.substr(testStart);
    }
    
    referralRows = referralSubstring.split('|');
    testRows = testSubstring.split('|');

    for (const row of referralRows) {
      if (row !== '') {
        const details = row.split('^');
        const referralItem = {
          description: details[0],
          date: details[1] ? details[1] : '',
          specialty: details[2] ? details[2] : '',
          provider: details[3] ? details[3] : '',
          tooltip: null       
        }
        
        referralItem.tooltip = 'Referral\n' + referralItem.description + '\n' + referralItem.provider + '\n' + referralItem.specialty + '\n' + referralItem.date;
        
        this.patientReferalOrderList.push(referralItem);
      }
    }
    
    for (const row of testRows) {
      if (row !== '') {
        const details = row.split('^');
        const testItem = {
          description: details[0],
          date: details[1] ? details[1] : '',
          specialty: details[2] ? details[2] : '',
          provider: details[3] ? details[3] : '',
          tooltip: null
        }
        
        testItem.tooltip = 'Test\n' + testItem.description + '\n' + testItem.provider + '\n' + testItem.specialty + '\n' + testItem.date;
        
        this.patientTestOrderList.push(testItem);
      }
    }
  }
 }
