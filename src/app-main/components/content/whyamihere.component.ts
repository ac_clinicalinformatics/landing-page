import { Component } from '@angular/core';
// import { Observable, Subject } from 'rxjs/Rx';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-whyamihere',
  templateUrl: './whyamihere.component.html',
  styleUrls: ['./whyamihere.component.css']
})
export class WhyAmIHereComponent {
  formId = 0;  
  chiefComplaint = '';
  savedChiefComplaint = '';
  chiefComplaintTimer = null;  
//  public textAreaKeyUp = new Subject<string>();
  
  constructor(private centricityApi: CentricityApiService) {
    this.formId = centricityApi.getFormId();
    this.chiefComplaint = centricityApi.evaluateMel('{OBSNOW("Chief Cmplnt")}');             
   
//    const observable = this.textAreaKeyUp
//      .debounceTime(1500)
//      .distinctUntilChanged()
//      .flatMap((commit) => {
//        return Observable.of(commit).delay(500);
//      })
//      .subscribe((data) => {
//        this.centricityApi.setObsValue('Chief Cmplnt', this.chiefComplaint);
//      });    
  }
  
  commitChiefComplaintChanges(obsValue) {
    if (obsValue !== this.savedChiefComplaint) {
      this.savedChiefComplaint = obsValue;
      this.centricityApi.setObsValue('Chief Cmplnt', obsValue);
    }
  }
  
  onChiefComplaintChange(changedText) {
    this.chiefComplaint = changedText; 
         
    clearTimeout(this.chiefComplaintTimer);    
    this.chiefComplaintTimer = setTimeout(() => this.commitChiefComplaintChanges(this.chiefComplaint), 1500);       
  }    
}
