import { Component } from '@angular/core';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-meds',
  templateUrl: './meds.component.html',
  styleUrls: ['./meds.component.css']
})
export class MedsComponent {
  title = 'meds';
  patientMedicationsList = [];
  patientPrescriptionsList = [];
  providerRenewalRequests: any = null;
  currentUserPvid = 0;  
  patientId = NaN;
  medRefillRequestCount = 0;
  
  constructor(private centricityApi: CentricityApiService) {
    this.currentUserPvid = centricityApi.getCurrentUserInfo().PVID;
    this.patientId = centricityApi.getPatientId();       
    
    this.loadPatientMedications(false);
    this.getProviderRenewalRequests();
    this.centricityApi.registerForMedicationNotifications(this.onMedsChangedCallback.bind(this));    
  }
  
  // Meds callback
  onMedsChangedCallback() {    
    this.loadPatientMedications(true);
  }  
  
  loadPatientMedications(getLatest) {    
    this.patientPrescriptionsList = this.centricityApi.getPatientPrescriptions(getLatest);
    
    this.patientMedicationsList.splice(0, this.patientMedicationsList.length);    
    const patientMedications = this.centricityApi.getPatientMedications(getLatest);       
    
    if(patientMedications) {    
      // Sort based on the userSort field
      patientMedications.sort((leftSide, rightSide): number => {
        if (+leftSide.userSort < +rightSide.userSort) {
         return -1; 
        }
        if (+leftSide.userSort > +rightSide.userSort) {
         return 1; 
        }
        return 0;
      });
      
      for (const med of patientMedications) {
        if (this.centricityApi.isSignedOrInCurrentUpdateItem(med) && this.centricityApi.isActiveItem(med)) {
          const lastRx = this.getLastSignedPrescriptionForMedication(med.medicationID);
          let lastRxString = '';
          if(lastRx) {          
            lastRxString = 'Last Rx: ' + this.centricityApi.jsonDateToString(lastRx.clinicalDate) + ' - ' + lastRx.providerDetail.firstName + ' ' + lastRx.providerDetail.lastName;
          }
          
          const medListItem = { 
            description: med.medicationInfoDetail ? med.medicationInfoDetail.productname : med.description,
            instructions: med.instructions,
            lastRxDetails: lastRxString
            };          
          
          this.patientMedicationsList.push(medListItem);
        }
      }
    }
  }
  
  renewalRequestsCallback(results) {
    this.providerRenewalRequests = results;
    this.medRefillRequestCount = 0;
    
    if (this.providerRenewalRequests && this.providerRenewalRequests.patientsRenewalsCount) {
      for (const renewalCount of this.providerRenewalRequests.patientsRenewalsCount) {
        if (renewalCount.pid === this.patientId) {           
          this.medRefillRequestCount = renewalCount.incomingRequestsCount;
        }
      }
    }    
  }
  
  testForRecentPrescription(lastPrescription, currentPrescription) {
      // Ignore unsigned prescriptions.
      if (currentPrescription.change != 2) {
          return lastPrescription;
      }
      if (lastPrescription == null) {
          return currentPrescription;
      }

      var lastDate = this.centricityApi.clinicalDateToString(lastPrescription.clinicalDate);
      var currentDate = this.centricityApi.clinicalDateToString(currentPrescription.clinicalDate);
      var last = Date.parse(lastDate);
      var current = Date.parse(currentDate);

      return last > current ? lastPrescription : currentPrescription;
  }    
  
  getLastSignedPrescriptionForMedication(medicationID) {
    let lastSignedRx = null;
    
    for (const prescription of this.patientPrescriptionsList) {
      if (prescription.medicationID == medicationID) {        
        if (prescription.erxDenialReasonCode && prescription.erxDenialReasonCode !== '') {
          continue;
        }
        
        lastSignedRx = this.testForRecentPrescription(lastSignedRx, prescription);
      }
    }
    
    return lastSignedRx;
  }
  
  getProviderRenewalRequests() {
    const pvids = [ this.currentUserPvid ];
    this.centricityApi.getRenewalRequestsAndCallback(pvids, this.renewalRequestsCallback.bind(this));
  }  
}
