import { TestBed, async } from '@angular/core/testing';

import { AboutMeComponent } from './aboutme.component';

describe('AboutMeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AboutMeComponent
      ],
    }).compileComponents();
  }));

  it('should create the aboutme', async(() => {
    const fixture = TestBed.createComponent(AboutMeComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'aboutme'`, async(() => {
    const fixture = TestBed.createComponent(AboutMeComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('aboutme');
  }));
});
