import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './meds.component';

describe('MedsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MedsComponent
      ],
    }).compileComponents();
  }));

  it('should create the activeissues', async(() => {
    const fixture = TestBed.createComponent(MedsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'activeissues'`, async(() => {
    const fixture = TestBed.createComponent(MedsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('activeissues');
  }));

  it('should render title in a h2 tag', async(() => {
    const fixture = TestBed.createComponent(MedsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Angular2 meds!!');
  }));
});
