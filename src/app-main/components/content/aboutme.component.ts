import { Component } from '@angular/core';
// import { Observable, Subject } from 'rxjs/Rx';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-aboutme',
  templateUrl: './aboutme.component.html',
  styleUrls: ['./aboutme.component.css']
})
export class AboutMeComponent {
  formId = 0;
  currentRating = NaN;
  aboutMe = '';
  savedAboutMe = '';
  aboutMeTimer = null;  
  patientDemographics: any = null;
//  public textAreaKeyUp = new Subject<string>();

  constructor(private centricityApi: CentricityApiService) {
    this.formId = centricityApi.getFormId();
    this.currentRating = +centricityApi.evaluateMel('{OBSANY("SELFHLTHRATE")}');
    this.aboutMe = centricityApi.evaluateMel('{OBSANY("LANPAGABTME")}');
  this.patientDemographics = centricityApi.getPatientDemographics(false);

//    const observable = this.textAreaKeyUp
//      .debounceTime(1500)
//      .distinctUntilChanged()
//      .flatMap((commit) => {
//        return Observable.of(commit).delay(500);
//      })
//      .subscribe((data) => {
//        this.centricityApi.setObsValue('LANPAGABTME', this.aboutMe);
//      });
  }
  
  onRatingChange(sliderChangeEvent) {
    this.centricityApi.setObsValue('SELFHLTHRATE', sliderChangeEvent.value.toString());
  }
  
  commitAboutMeChanges(obsValue) {
    if (obsValue !== this.savedAboutMe) {
      this.savedAboutMe = obsValue;
      this.centricityApi.setObsValue('LANPAGABTME', obsValue);
    }
  }  
  
  onAboutMeChange(changedText) {
    this.aboutMe = changedText; 
      
    clearTimeout(this.aboutMeTimer);    
    this.aboutMeTimer = setTimeout(() => this.commitAboutMeChanges(this.aboutMe), 1500);                
  }
}
