import { TestBed, async } from '@angular/core/testing';

import { VitalsComponent } from './vitals.component';

describe('VitalsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        VitalsComponent
      ],
    }).compileComponents();
  }));

  it('should create the vitals', async(() => {
    const fixture = TestBed.createComponent(VitalsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'vitals'`, async(() => {
    const fixture = TestBed.createComponent(VitalsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('vitals');
  }));
});
