import { Component } from '@angular/core';
// import { Observable, Subject } from 'rxjs/Rx';

import { CentricityApiService } from '../../services/centricity_api.service'


@Component({
  selector: 'app-notestoself',
  templateUrl: './notestoself.component.html',
  styleUrls: ['./notestoself.component.css']
})
export class NotesToSelfComponent {
  
  formId = 0;
  notesToSelf = '';
  savedNotesToSelf = '';
  notesToSelfTimer = null;  
//  public textAreaKeyUp = new Subject<string>();

  constructor(private centricityApi: CentricityApiService) {
    this.formId = centricityApi.getFormId();
    this.notesToSelf = centricityApi.evaluateMel('{OBSANY("LANPAPROVNT")}');
    
//    const observable = this.textAreaKeyUp
//      .debounceTime(1500)
//      .distinctUntilChanged()
//      .flatMap((commit) => {
//        return Observable.of(commit).delay(500);
//      })
//      .subscribe((data) => {
//        this.centricityApi.setObsValue('LANPAPROVNT', this.notesToSelf);
//      });    
  }
  
  commitNotesToSelfChanges(obsValue) {
    if (obsValue !== this.savedNotesToSelf) {
      this.savedNotesToSelf = obsValue;
      this.centricityApi.setObsValue('LANPAPROVNT', obsValue);
    }
  }    
  
  onNotesToSelfChange(changedText) {
    this.notesToSelf = changedText;
    
    clearTimeout(this.notesToSelfTimer);    
    this.notesToSelfTimer = setTimeout(() => this.commitNotesToSelfChanges(this.notesToSelf), 1500);                
  }  

//clear obsterm value of NotesToSelf
 public EraseNTS() {
    this.notesToSelf = '';
 }
  
//set obsterm value from previous NotesToSelf
 bringPrevNTS(){
   this.notesToSelf += '\n' + this.centricityApi.evaluateMel('{OBSPREV("LANPAPROVNT")}') + '\t' + this.centricityApi.evaluateMel('{LAST_SIGNED_OBS_DATE("LANPAPROVNT")}')
 } 
}
