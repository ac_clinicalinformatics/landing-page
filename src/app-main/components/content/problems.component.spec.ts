import { TestBed, async } from '@angular/core/testing';

import { ProblemsComponent } from './problems.component';

describe('ProblemsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProblemsComponent
      ],
    }).compileComponents();
  }));

  it('should create the problems', async(() => {
    const fixture = TestBed.createComponent(ProblemsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'problems'`, async(() => {
    const fixture = TestBed.createComponent(ProblemsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('problems');
  }));


});
