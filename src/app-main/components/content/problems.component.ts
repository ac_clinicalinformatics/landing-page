import { Component , ApplicationRef } from '@angular/core';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-problems',
  templateUrl: './problems.component.html',
  styleUrls: ['./problems.component.css']
})
export class ProblemsComponent {
  title = 'problems';
  patientProblemsList = [];
  
  constructor(private centricityApi: CentricityApiService , private ref: ApplicationRef) {
    this.loadPatientProblems(false);
    this.centricityApi.registerForProblemNotifications(this.onProblemsChangedCallback.bind(this));    
  }
  
  // Toggle Problem Decision Dropdown
   toggleProblemDropdown() {
    document.getElementById("ProbDecDropdown").classList.toggle("show");
  }
  
  
  // Problems callback
  onProblemsChangedCallback() {
    this.loadPatientProblems(true);
  }
  
  // remove single problem from Patient -> callback  
  remPatientProblem(ProbID){
    var temp;
    this.centricityApi.evaluateMel('{MEL_REMOVE_PROBLEM(\''+ProbID+'\')}');
    //temp = this.centricityApi.evaluateMel('{MEL_REMOVE_PROBLEM(\''+ProbID+'\')}');
    //alert(temp);
    this.loadPatientProblems(true);
    // this.ref.tick();
  }

  loadPatientProblems(loadLatest) { 
    this.patientProblemsList.splice(0, this.patientProblemsList.length);
    
    const patientProblems = this.centricityApi.getPatientProblems(loadLatest);
    
    for (const problem of patientProblems) {
      if (this.centricityApi.isSignedOrInCurrentUpdateItem(problem) && this.centricityApi.isActiveItem(problem)) {
        let assessmentList = [];    
        let annotate = null;    
        let clinicaldate = null;
        let providerName = null;
        let tooltipText = null;
        
        if (problem.assessmentList && problem.assessmentList.length > 0) {
          
          for (const assessment of problem.assessmentList) {
            // Only signed problems not expired
            if (assessment.change === 2) {
              const assessmentItem = { 
                 annotate: assessment.annotate ? assessment.annotate : '',
                 clinicaldate: this.centricityApi.jsonDateToString((assessment.clinicaldate)),
                 providerName: assessment.pubUserDetail.firstName + ' ' + assessment.pubUserDetail.lastName
              };
              
              assessmentList.push(assessmentItem);
              
              if(tooltipText == null) {
                tooltipText = assessmentItem.annotate + '\r' + assessmentItem.providerName + ' on ' + assessmentItem.clinicaldate;
              }
            }
          }
        }
        const problemListItem = { description: problem.description, assessmentList : assessmentList, tooltipText : tooltipText , PRID : problem.problemID };
        this.patientProblemsList.push(problemListItem);
      }
    }
  }
}
