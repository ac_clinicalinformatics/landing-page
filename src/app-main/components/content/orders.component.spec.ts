import { TestBed, async } from '@angular/core/testing';

import { OrdersComponent } from './orders.component';

describe('OrdersComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrdersComponent
      ],
    }).compileComponents();
  }));

  it('should create the orders', async(() => {
    const fixture = TestBed.createComponent(OrdersComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'orders'`, async(() => {
    const fixture = TestBed.createComponent(OrdersComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('orders');
  }));
});
