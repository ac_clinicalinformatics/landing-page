import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './alerts.component';

describe('AlertsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ActiveIssuesComponent
      ],
    }).compileComponents();
  }));

  it('should create the alerts', async(() => {
    const fixture = TestBed.createComponent(AlertsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'activeissues'`, async(() => {
    const fixture = TestBed.createComponent(AlertsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('alerts');
  }));

  it('should render title in a h2 tag', async(() => {
    const fixture = TestBed.createComponent(AlertsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Angular2 alerts!!');
  }));
});
