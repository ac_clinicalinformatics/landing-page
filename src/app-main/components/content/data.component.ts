import { ApplicationRef, Component } from '@angular/core';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})

export class DataComponent {
  title = 'data';
  // Prev Values 
  ListAllResultLimit = 5;
  
  // Most Recent Lab Values 
  mostRecentHDL: string = null;
  mostRecentLDL: string = null;
  mostRecentTotChol: string = null;
  mostRecentTG: string = null;
  mostRecentA1c: string = null;
  mostRecentTC: string = null;
  mostRecentCr: string = null;
  mostRecentHgb: string = null;
  mostRecentTSH: string = null;
  mostRecentPHQ9: string = null;
  
  // Lab Value MM/YY string 
  RecentDateHDL: string = null;
  RecentDateLDL: string = null;
  RecentDateTotChol: string = null;
  RecentDateTG: string = null;
  RecentDateA1c: string = null;
  RecentDateTC: string = null;
  RecentDateCr: string = null;
  RecentDateHgb: string = null;
  RecentDateTSH: string = null;
  RecentDatePHQ9: string = null;
  
  
  public lineChartData1: Array<any> = [
    {data: [], label: 'TC'},
    {data: [], label: 'HDL'},
    {data: [], label: 'LDL'},
    {data: [], label: 'TG'},
  ];

  public lineChartData2: Array<any> = [
    {data: [], label: 'A1c'},
    {data: [], label: 'TC'},
    {data: [], label: 'Cr'},
    {data: [], label: 'Hgb'},
    {data: [], label: 'TSH'},
  ];

  public lineChartData3: Array<any> = [
    {data: [], label: 'PHQ9'}
  ]
  
  dataLoadItemsRemaining1 = this.lineChartData1.length; 
  dataLoadItemsRemaining2 = this.lineChartData2.length;   
  dataLoadItemsRemaining3 = this.lineChartData3.length; 
  
  public lineChartLabels1: Array<any> = [];
  public lineChartLabels2: Array<any> = [];
  public lineChartLabels3: Array<any> = [];
  
  public lineChartOptions: any = {
    responsive: true, 
    maintainAspectRatio: false,
    legend: {
      position: 'bottom',
      labels: {
      boxWidth: 16,
      fontSize: 9
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          fontSize: 9
        },
        type: 'time',
        time: {
          displayFormats: {
            quarter: 'MMM YYYY'
          },
          tooltipFormat: 'MM/DD/YYYY'
        }
      }],
      yAxes: [{
        ticks: {
          fontSize: 9
        }
      }]
    }
  };
  
  public lineChartColors: Array<any> = [
    { // blue
      backgroundColor: 'transparent',
      borderColor: 'rgba(27,143,244,1)',
      pointBackgroundColor: 'rgba(27,143,244,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(27,143,244,0.8)'
    },
    { // purple
      backgroundColor: 'transparent',
      borderColor: 'rgba(189,16,224,1)',
      pointBackgroundColor: 'rgba(189,16,224,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(189,16,224,1)'
    },
    { // orange
      backgroundColor: 'transparent',
      borderColor: 'rgba(241,164,38,1)',
      pointBackgroundColor: 'rgba(241,164,38,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(241,164,38,0.8)'
    },
    { // green
      backgroundColor: 'transparent',
      borderColor: 'rgba(126,211,33,1)',
      pointBackgroundColor: 'rgba(126,211,33,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(126,211,33,0.8)'
    },
    { // seagreen
      backgroundColor: 'transparent',
      borderColor: 'rgba(30,161,178,1)',
      pointBackgroundColor: 'rgba(30,161,178,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(30,161,178,0.8)'
    },
    { // orange
      backgroundColor: 'transparent',
      borderColor: 'rgba(255,127,39,1)',
      pointBackgroundColor: 'rgba(255,127,39,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255,127,39,0.8)'
    },
    { // brown
      backgroundColor: 'transparent',
      borderColor: 'rgba(128,64,64,1)',
      pointBackgroundColor: 'rgba(128,64,64,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(128,64,64,0.8)'
    }
  ];
  
  public lineChartLegend = true;
  public lineChartType = 'line';
  
  constructor(private ref: ApplicationRef, private centricityApi: CentricityApiService, private appRef: ApplicationRef) {
    this.buildAllData();
  }
  
  // Private functions
  private async buildAllData(): Promise<void> {
    //Lipid
    this.centricityApi.listSignedObsDelim('HDL', this.buildHDLChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('LDL', this.buildLDLChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('Cholesterol', this.buildTotalCholChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('Triglyceride', this.buildTCCallback.bind(this));    
    //Other
    this.centricityApi.listSignedObsDelim('Creatinine', this.buildCreatinineChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('Hgb', this.buildHgbChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('HgbA1c', this.buildA1cChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('TSH', this.buildTSHChartCallback.bind(this));
    //Other Other  
    this.centricityApi.listSignedObsDelim('PHQ-9 Score', this.buildPHQ9ChartCallback.bind(this));
    
    this.getMostRecentLabValue_Date();
  }
  
  private constructDatasetForCharting(rows , chartSel) {
    const resultData = [];
    let count = 0;

    // Order of rows comes back as Newest to Oldest
    for (const row of rows) {
      if (count >= this.ListAllResultLimit) {
      break;
      }
      count++;

      const valueDateTime = row.split('^');
      const value = +valueDateTime[0];

      // Push valid value into dataset
      if (value !== NaN) {

      // Get the date and adds if not already in labels
      const date = valueDateTime[1];
      const dateAndTime = new Date(valueDateTime[1] + ' ' + valueDateTime[2]);
      const dateAndTimeInTicks = dateAndTime.getTime();

      // Setup the labels
      if (date) {
        if (dateAndTimeInTicks == NaN) {
        // Invalid dates cannot be graphed
        console.log('Invalid OBS date format skipped: ' + date);
        continue;
        }
      } else {
        console.log('Null OBS date skipped');
        continue;
      }

      const datapoint = { y: value, x: dateAndTimeInTicks };
      resultData.push(datapoint);
      }
    }
    // Decrement load items remaining
    // Switch added for multiple charts
    switch (chartSel){
      case 'Lipid': {
        this.dataLoadItemsRemaining1--;
        break;
      }
      
      case 'Other': { 
        this.dataLoadItemsRemaining2--; 
        break;
      }
        
      case 'MH': {
        this.dataLoadItemsRemaining3--;
        break;
      }
      
      default: {break}
    }
    
    return resultData;
  } 
  
    // functions
  async buildTCCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData1.findIndex( function(x) { return x.label === 'TC' } );
    const rows = listResults.split('|');

    this.lineChartData1[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Lipid');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining1 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData1));
      this.lineChartData1 = clone;
    }
  }
  
  async buildHDLChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData2.findIndex( function(x) { return x.label === 'HDL' } );
    const rows = listResults.split('|');

    this.lineChartData2[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Lipid');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining2 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData2));
      this.lineChartData2 = clone;
    }
  }

  async buildA1cChartCallback(listResults): Promise<void> {
    
    const dataSetIndex = this.lineChartData2.findIndex( function(x) { return x.label === 'A1c' } );
  const rows = listResults.split('|');

    this.lineChartData2[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Other');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining2 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData2));
      this.lineChartData2 = clone;
    }
  }

  async buildHgbChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData2.findIndex( function(x) { return x.label === 'Hgb' } );
    const rows = listResults.split('|');

    this.lineChartData2[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Other');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining2 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData2));
      this.lineChartData2 = clone;
    }
  }

  async buildCreatinineChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData2.findIndex( function(x) { return x.label === 'Cr' } );
    const rows = listResults.split('|');

    this.lineChartData2[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Other');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining2 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData2));
      this.lineChartData2 = clone;
    }
  }

  async buildTSHChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData2.findIndex( function(x) { return x.label === 'TSH' } );
    const rows = listResults.split('|');

    this.lineChartData2[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Other');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining2 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData2));
      this.lineChartData2 = clone;
    }
  }

  async buildPHQ9ChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData3.findIndex( function(x) { return x.label === 'PHQ9' } );
    const rows = listResults.split('|');

    this.lineChartData3[dataSetIndex].data = this.constructDatasetForCharting(rows,'MH');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining3 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData3));
      this.lineChartData3 = clone;
    }
  }
  
  async buildLDLChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData1.findIndex( function(x) { return x.label === 'LDL' } );
    const rows = listResults.split('|');

    this.lineChartData1[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Lipid');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining1 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData1));
      this.lineChartData1 = clone;
    }
  }
  
  async buildTotalCholChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.lineChartData1.findIndex( function(x) { return x.label === 'TCHOL' } );
    const rows = listResults.split('|');

    this.lineChartData1[dataSetIndex].data = this.constructDatasetForCharting(rows , 'Lipid');

    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */

    if (this.dataLoadItemsRemaining1 <= 0) {
      const clone = JSON.parse(JSON.stringify(this.lineChartData1));
      this.lineChartData1 = clone;
    }
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }
  public chartHovered(e: any): void {
    console.log(e);
  }
  
  //Flex View Function 
 public openChartView(evt, ChartView , tabViews) {
    var i, tabcontent, tablinks , tabViewContent;
    tabcontent = document.getElementsByClassName("chart-container");
    tabViewContent = document.getElementsByClassName("chart-container-table");
    tablinks = document.getElementsByClassName("Chartlinks");  
    
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    for (i = 0; i < tabViewContent.length; i++) {
        tabViewContent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("Chartlinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("active", "");
    }
    
    document.getElementById(ChartView).style.display = "inline-block";
    document.getElementById(tabViews).style.display = "inline-block";
    
    evt.currentTarget.className += " active";
    
  }
  //Make's Test view default show. 
 public defaultTab(){
    document.getElementById("DefaultChartView").click();
 }
  
 //Code for Data sidebar   
  getMostRecentLabValue_Date() {
      //Lipids
      this.centricityApi.recentObs('HDL', this.labMostRecentHDL.bind(this));
      this.centricityApi.recentObs('LDL', this.labMostRecentLDL.bind(this));
      this.centricityApi.recentObs('Cholesterol', this.labMostRecentTotChol.bind(this));
      this.centricityApi.recentObs('Triglyceride', this.labMostRecentTrig.bind(this));
       //Lipids Date
      this.centricityApi.recentObs_date('HDL', this.labMostRecentHDL_date.bind(this));
      this.centricityApi.recentObs_date('LDL', this.labMostRecentLDL_date.bind(this));
      this.centricityApi.recentObs_date('Cholesterol', this.labMostRecentTotChol_date.bind(this));
      this.centricityApi.recentObs_date('Triglyceride', this.labMostRecentTrig_date.bind(this));
      //Other
      this.centricityApi.recentObs('Creatinine', this.labMostRecentCreat.bind(this));
      this.centricityApi.recentObs('Hgb', this.labMostRecentHgb.bind(this));
      this.centricityApi.recentObs('HgbA1c', this.labMostRecentA1c.bind(this));
      this.centricityApi.recentObs('TSH', this.labMostRecentTSH.bind(this));
      //Other Date
      this.centricityApi.recentObs_date('Hgb', this.labMostRecentHgb_date.bind(this));
      this.centricityApi.recentObs_date('HgbA1c', this.labMostRecentA1c_date.bind(this));
      this.centricityApi.recentObs_date('TSH', this.labMostRecentTSH_date.bind(this));
      this.centricityApi.recentObs_date('Creatinine', this.labMostRecentCreat_date.bind(this));
      //MH
      this.centricityApi.recentObs('PHQ-9 Score', this.labMostRecentPHQ9.bind(this));
      //MH Date
      this.centricityApi.recentObs_date('PHQ-9 Score', this.labMostRecentPHQ9_date.bind(this));
   } 

 labMostRecentHDL(value) {
    this.mostRecentHDL = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentLDL(value) {
    this.mostRecentLDL = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentTotChol(value) {
    this.mostRecentTotChol = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentTrig(value) {
    this.mostRecentTG = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentCreat(value) {
    this.mostRecentCr = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentHgb(value) {
    this.mostRecentHgb = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentA1c(value) {
    this.mostRecentA1c = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentTSH(value) {
    this.mostRecentTSH = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentPHQ9(value) {
    this.mostRecentPHQ9 = value !== '' ? value : '-';
    this.appRef.tick();
 }
  labMostRecentHDL_date(value) {
    this.RecentDateHDL = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentLDL_date(value) {
    this.RecentDateLDL = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentTotChol_date(value) {
    this.RecentDateTotChol = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentTrig_date(value) {
    this.RecentDateTG = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentCreat_date(value) {
    this.RecentDateCr = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentHgb_date(value) {
    this.RecentDateHgb = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentA1c_date(value) {
    this.RecentDateA1c = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentTSH_date(value) {
    this.RecentDateTSH = value !== '' ? value : '-';
    this.appRef.tick();
 }
 labMostRecentPHQ9_date(value) {
    this.RecentDatePHQ9 = value !== '' ? value : '-';
    this.appRef.tick();
 } 
   
}
  
  
  
  
  
  
  
  
  
  
  
