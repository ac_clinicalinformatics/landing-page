import { Component, ViewChild, ElementRef } from '@angular/core';

import { CentricityApiService } from '../../services/centricity_api.service'

// Javascript library
declare var vis: any;

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent {
  patientId = NaN;  
  minTimelineDate = new Date();
  maxTimelineDate = new Date();
  
  @ViewChild('timeline') patientTimeline: ElementRef;  
  
  constructor(private element: ElementRef, private centricityApi: CentricityApiService) {
    this.patientId = centricityApi.getPatientId();  
  }  
  
  ngOnInit() {    
        
    const now = new Date();
    
    // Min timeline date default two weeks into future
    this.maxTimelineDate.setDate(now.getDate() + 14);    
    
    // Min timeline date default one week earlier
    this.minTimelineDate.setDate(now.getDate() - 7);
    
    const timelineData = this.getTimelineData();     
    
    // Configuration for the Timeline
    const options = {
      width: '100%',
      height: '90px',
      min: this.minTimelineDate,
      max: this.maxTimelineDate,
      start: new Date(now.getFullYear() - 1, 0, 0),
      end: this.maxTimelineDate,
      zoomMax: 315360000000,
      zoomMin: 86400000
    };       
    
    // Create a Timeline
    var timeline = new vis.Timeline(this.patientTimeline.nativeElement, timelineData, options);    
  }
  
  getTimelineData() {
    var items = [];
    var index = 1;
    
    if (this.patientId !== NaN && this.patientId > 0) {
      const appointmentsList = this.centricityApi.getPreviousAndNextAppointmentListForPatient(this.patientId, 5, 1);

      if (appointmentsList.length > 0) {
        let timelineEvent: any = null;              

        for (const appt of appointmentsList) {
          const currentApptStart = new Date(appt.apptStart);
          
          var tt_date = currentApptStart.getDate();
          var tt_month = currentApptStart.getMonth()+1;
          var tt_year = currentApptStart.getFullYear();            
          var tt_final = tt_month + '/' + tt_date + '/' + tt_year ;
          
          if(currentApptStart < this.minTimelineDate) {
            // Track min timeline date as one week earlier
            this.minTimelineDate.setDate(0);
            this.minTimelineDate.setFullYear(currentApptStart.getFullYear() - 1);
          }
          
          let popoverText = '';          
          
          popoverText = tt_final + ' - ' + appt.apptTypeName + '\n' + appt.doctorName;
          
          if (appt.documentSummary) {
            popoverText += '\nDoc Summary: ' + appt.documentSummary;
          }     
          
          if (this.centricityApi.getPtProbonSpecDate(tt_final)) {
              popoverText += '\n'+this.centricityApi.getPtProbonSpecDate(tt_final) ;
           }
          
          
          const apptContextHtml = '<div class = "test" title="' + popoverText + '">O</div>';     
          
          timelineEvent = { id: index++, start: currentApptStart, content: apptContextHtml };

          items.push(timelineEvent);         
        }
      }
    }
    
    return new vis.DataSet(items);
  }  
}
