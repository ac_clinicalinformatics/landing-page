import { TestBed, async } from '@angular/core/testing';

import { NotesToSelfComponent } from './notestoself.component';

describe('NotesToSelfComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NotesToSelfComponent
      ],
    }).compileComponents();
  }));

  it('should create the notestoself', async(() => {
    const fixture = TestBed.createComponent(NotesToSelfComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'notestoself'`, async(() => {
    const fixture = TestBed.createComponent(NotesToSelfComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('notestoself');
  }));
});
