import { Component } from '@angular/core';

import { CentricityRulesService } from '../../services/centricity_rules.service'
import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
  
export class AlertsComponent {
  title = 'alerts';
  careActionList = [];
  careAlertCount = 0;

  constructor(private rulesService: CentricityRulesService, private apiService: CentricityApiService) {

    // Evaluate rule on load
    this.careAlertCount = 0;
    this.rulesService.loadRuleset('adultpreventative', this.runAdultPreventativeRules.bind(this), 0, 0);
    this.rulesService.loadRuleset('adultpreventativewarn', this.runAdultPreventativeWarnRules.bind(this), 0, 0);    
  }

  runAdultPreventativeRules(ruleset) {
    for (const rule of ruleset.data) {
      const result = this.rulesService.runMelDataRule(rule.jsonLogic.rule, rule.jsonLogic.meldatatemplate, rule.jsonLogic.melprecursorfunc);
      if (result) {
        // True eval so add button to display
        const alertTooltip = this.apiService.evaluateMel(rule.hoverTool);
        this.careActionList.push({ 'action_name' : rule.action_name, 'actions' : rule.actions, 'cssBorder': rule.cssBorder, 'tooltip': alertTooltip });
      }
    }

    this.careAlertCount = this.careActionList.length;
  }

  runAdultPreventativeWarnRules(ruleset) {
    for (const rule of ruleset.data) {
      const result = this.rulesService.runMelDataRule(rule.jsonLogic.rule, rule.jsonLogic.meldatatemplate, rule.jsonLogic.melprecursorfunc);
      if (result) {
        // True eval so add button to display
        const alertTooltip = this.apiService.evaluateMel(rule.hoverTool);
        this.careActionList.push({ 'action_name' : rule.action_name, 'actions' : rule.actions, 'cssBorder': rule.cssBorder, 'tooltip': alertTooltip });
      }
    }

    this.careAlertCount = this.careActionList.length;
  }  
  
}
