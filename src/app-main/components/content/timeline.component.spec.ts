import { TestBed, async } from '@angular/core/testing';

import { TimelineComponent } from './timeline.component';

describe('TimelineComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TimelineComponent
      ],
    }).compileComponents();
  }));

  it('should create the timeline', async(() => {
    const fixture = TestBed.createComponent(TimelineComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'timeline'`, async(() => {
    const fixture = TestBed.createComponent(TimelineComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('timeline');
  }));

  it('should render title in a h2 tag', async(() => {
    const fixture = TestBed.createComponent(TimelineComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Angular2 timeline!!');
  }));
});
