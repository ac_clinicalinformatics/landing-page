import { TestBed, async } from '@angular/core/testing';

import { AppWhyAmIHereComponent } from './app.whyamihere.component';

describe('WhyAmIHereComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppWhyAmIHereComponent
      ],
    }).compileComponents();
  }));

  it('should create the whyamihere', async(() => {
    const fixture = TestBed.createComponent(WhyAmIHereComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'whyamihere'`, async(() => {
    const fixture = TestBed.createComponent(WhyAmIHereComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('whyamihere');
  }));
});
