import { TestBed, async } from '@angular/core/testing';

import { DataComponent } from './data.component';

describe('DataComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ResultsComponent
      ],
    }).compileComponents();
  }));

  it('should create the data', async(() => {
    const fixture = TestBed.createComponent(DataComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'data'`, async(() => {
    const fixture = TestBed.createComponent(DataComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('data');
  }));
});
