import { ApplicationRef, Component } from '@angular/core';

import { CentricityApiService } from '../../services/centricity_api.service'

@Component({
  selector: 'app-vitals',
  templateUrl: './vitals.component.html',
  styleUrls: ['./vitals.component.css']
})
export class VitalsComponent {
  title = 'vitals';
  ListAllResultLimit = 5;
  quickSummaryTooltip = '';
  
  //Vital Signs Sidebar
  todayHeight: string = null;
  todayBMI: string = null;
  todayWeight: string = null;
  todayDiastolic: string = null;
  todaySystolic: string = null;
  todayPulseRate: string = null;
  todayTemp: string = null;
  todayO2Sat: string = null;
  todayRespRate: string = null;
  
  // lineChart
  public vitalsChartData: Array<any> = [
    {data: [], label: 'SBP'},
    {data: [], label: 'DBP'},
    {data: [], label: 'HR'},
    {data: [], label: 'Wt'},
    {data: [], label: 'BMI'}
  ];

  dataLoadItemsRemaining = this.vitalsChartData.length;
  
  public vitalsChartLabels: Array<any> = [];

  public vitalsChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom',
      labels: {
        boxWidth: 16,
        fontSize: 9
      }
    },
    scales: {
        xAxes: [{
            ticks: {
                fontSize: 9
            },
            type: 'time',
            time: {
                displayFormats: {
                    quarter: 'MMM YYYY'
                },
              tooltipFormat: 'MM/DD/YYYY'
            }
        }],
        yAxes: [{
            ticks: {
                fontSize: 9
            }
        }]
    },
    tooltips: {
        callbacks: {

        }
    }
  };

  public lineChartColors: Array<any> = [
    { // blue
      backgroundColor: 'transparent',
      borderColor: 'rgba(27,143,244,1)',
      pointBackgroundColor: 'rgba(27,143,244,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(27,143,244,0.8)'
    },
    { // purple
      backgroundColor: 'transparent',
      borderColor: 'rgba(189,16,224,1)',
      pointBackgroundColor: 'rgba(189,16,224,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(189,16,224,1)'
    },
    { // red
      backgroundColor: 'transparent',
      borderColor: 'rgba(255,0,0,1)',
      pointBackgroundColor: 'rgba(255,0,0,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255,0,0,0.8)'
    },    
    { // orange
      backgroundColor: 'transparent',
      borderColor: 'rgba(241,164,38,1)',
      pointBackgroundColor: 'rgba(241,164,38,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(241,164,38,0.8)'
    },
    { // green
      backgroundColor: 'transparent',
      borderColor: 'rgba(126,211,33,1)',
      pointBackgroundColor: 'rgba(126,211,33,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(126,211,33,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  constructor(private ref: ApplicationRef, private centricityApi: CentricityApiService ,private appRef: ApplicationRef) {    
    this.buildAllData();
    
    this.getObsAndAssignObsValues();
  }

  // private functions
  private async buildAllData(): Promise<void> {
    this.centricityApi.listSignedObsDelim('BP Systolic', this.buildSystolicBPChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('BP Diastolic', this.buildDiastolicBPChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('Weight', this.buildWeightChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('BMI', this.buildBMIChartCallback.bind(this));
    this.centricityApi.listSignedObsDelim('Pulse Rate', this.buildPulseRateChartCallback.bind(this));  
  }    
  
  private constructDatasetForCharting(rows) {
    const resultData = [];
    let count = 0;

    // Order of rows comes back as from Newest to Oldest
    for (const row of rows) {      
      if (count >= this.ListAllResultLimit) {
        break;
      }
      count++;

      const valueDateTime = row.split('^');
      const value = +valueDateTime[0];

      // Push valid value into dataset
      if (value !== NaN) {        

        // Get the date and adds if not already in labels
        const date = valueDateTime[1];
        const dateAndTime = new Date(valueDateTime[1] + ' ' + valueDateTime[2]);
        const dateAndTimeInTicks = dateAndTime.getTime();        
        
        // Setup the labels
        if (date) {
          if (dateAndTimeInTicks == NaN) {
            // Invalid dates cannot be graphed
            console.log('Invalid OBS date format skipped: ' + date);
            continue;
          }
        } else {
          console.log('Null OBS date skipped');
          continue;
        }
        
        const datapoint = { y: value, x: dateAndTimeInTicks };
        resultData.push(datapoint);
      }
    }   
    
    // Decrement load items remaining
    this.dataLoadItemsRemaining--;
    
    return resultData;
  }

  // functions
  constructQuickSummaryTooltip() {
    for (const dataRow of this.vitalsChartData) {      
      this.quickSummaryTooltip += dataRow.label + ' : ' +  dataRow.data.length + ' items\n';
    }
  }
  
  async buildWeightChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.vitalsChartData.findIndex( function(x) { return x.label === 'Wt' } );
    const rows = listResults.split('|');

    this.vitalsChartData[dataSetIndex].data = this.constructDatasetForCharting(rows);    
    
    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */       
    
    if (this.dataLoadItemsRemaining <= 0) {
      const clone = JSON.parse(JSON.stringify(this.vitalsChartData));      
      this.vitalsChartData = clone;
      this.constructQuickSummaryTooltip();
    }      
  }

  async buildDiastolicBPChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.vitalsChartData.findIndex( function(x) { return x.label === 'DBP' } );
    const rows = listResults.split('|');

    this.vitalsChartData[dataSetIndex].data = this.constructDatasetForCharting(rows);
    
    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */    
    if (this.dataLoadItemsRemaining <= 0) {
      const clone = JSON.parse(JSON.stringify(this.vitalsChartData));      
      this.vitalsChartData = clone;   
      this.constructQuickSummaryTooltip();
    }      
  }

  async buildSystolicBPChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.vitalsChartData.findIndex( function(x) { return x.label === 'SBP' } );
    const rows = listResults.split('|');

    this.vitalsChartData[dataSetIndex].data = this.constructDatasetForCharting(rows);
    
    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
    
    if (this.dataLoadItemsRemaining <= 0) {
      const clone = JSON.parse(JSON.stringify(this.vitalsChartData));      
      this.vitalsChartData = clone;
      this.constructQuickSummaryTooltip();   
    }      
  }

  async buildBMIChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.vitalsChartData.findIndex( function(x) { return x.label === 'BMI' } );
    const rows = listResults.split('|');

    this.vitalsChartData[dataSetIndex].data = this.constructDatasetForCharting(rows);
    
    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
    
    if (this.dataLoadItemsRemaining <= 0) {
      const clone = JSON.parse(JSON.stringify(this.vitalsChartData));      
      this.vitalsChartData = clone;   
      this.constructQuickSummaryTooltip();
    }      
  }
  
  async buildPulseRateChartCallback(listResults): Promise<void> {
    const dataSetIndex = this.vitalsChartData.findIndex( function(x) { return x.label === 'HR' } );
    const rows = listResults.split('|');
    
    this.vitalsChartData[dataSetIndex].data = this.constructDatasetForCharting(rows);
    
    /**
     * For Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */    
    
    if (this.dataLoadItemsRemaining <= 0) {
      const clone = JSON.parse(JSON.stringify(this.vitalsChartData));      
      this.vitalsChartData = clone;   
      this.constructQuickSummaryTooltip();
    }
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }
  public chartHovered(e: any): void {
    console.log(e);
  }
  //Code for Vital Signs Sidebar
  
  getObsAndAssignObsValues() {

      this.centricityApi.obsNow('Weight', this.onTodayWeightCallback.bind(this));
      this.centricityApi.obsNow('Height', this.onTodayHeightCallback.bind(this));
      this.centricityApi.obsNow('Bmi', this.onTodayBMICallback.bind(this));
  
      this.centricityApi.obsNow('Pulse Rate', this.onTodayPulseRateCallback.bind(this));
      this.centricityApi.obsNow('Temperature', this.onTodayTemperatureCallback.bind(this));
      this.centricityApi.obsNow('O2Sat(OXIM)', this.onTodayO2Callback.bind(this));
      this.centricityApi.obsNow('Resp Rate', this.onTodayRespRateCallback.bind(this));
      this.centricityApi.obsNow('BP Diastolic', this.onTodayBpDiastolicCallback.bind(this));
      this.centricityApi.obsNow('BP Systolic', this.onTodayBpSystolicCallback.bind(this));
   }
  
  // OBS callback
  onObsChangedCallback() {
    this.getObsAndAssignObsValues();
  }

  onTodayBpDiastolicCallback(value) {
    this.todayDiastolic = value !== '' ? value : '-';
    this.appRef.tick();
  }

  onTodayBpSystolicCallback(value) {
    this.todaySystolic = value !== '' ? value : '-';
    this.appRef.tick();
  }

  onTodayRespRateCallback(value) {
    this.todayRespRate = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayO2Callback(value) {
    this.todayO2Sat = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayPulseRateCallback(value) {
    this.todayPulseRate = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayTemperatureCallback(value) {
    this.todayTemp = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayBMICallback(value) {
    this.todayBMI = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayHeightCallback(value) {
    this.todayHeight = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }

  onTodayWeightCallback(value) {
    this.todayWeight = value !== '' ? value : 'n/a';
    this.appRef.tick();
  }


}
